# recipe-app-api-proxy

NGINX proxy app for recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (defaults: `app`)
* `APP_PORT` = Port of the app to forward requests to (default: `9000`)